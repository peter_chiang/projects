
    def distance_from(self, other):
        ''' Returns distance from current instance to other Point '''
        return math.hypot(other.x-self.x,other.y-self.y)

 	def __str__(self):
        ''' Returns string representing Distance object '''
        return "Distance from Point(x={},y={}) to Point(x={},y={}) is {}".format(self.x,self.y)