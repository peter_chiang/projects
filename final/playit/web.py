''' playit.web - Web Application '''

from playit.database import Database 

import sys
import json
import logging
import socket
import socket

import tornado.ioloop
import tornado.web

# Application

class Application(tornado.web.Application):
    DEFAULT_PORT  = 9874
    TEMPLATE_PATH = 'assets/html'

    def __init__(self, port=DEFAULT_PORT):
	tornado.web.Application.__init__(self, debug=True, template_path=Application.TEMPLATE_PATH)
        self.logger   = logging.getLogger()
        self.database = Database()
        self.ioloop   = tornado.ioloop.IOLoop.instance()
        self.port     = port

        # TODO: Add Index, Album, Artist, and Track Handlers
        self.add_handlers('', [
            (r'/'           , IndexHandler),
            (r'/search'     , SearchHandler),
            (r'/genre/(.*)', GenreHandler),
            (r'/year/(.*)' , YearHandler),
            (r'/assets/(.*)', tornado.web.StaticFileHandler, {'path': 'assets'}),
        ])

    def run(self):
        try:
            self.listen(self.port)
        except socket.error as e:
            self.logger.fatal('Unable to listen on {} = {}'.format(self.port, e))
            sys.exit(1)

        self.ioloop.start()

# Handlers

class IndexHandler(tornado.web.RequestHandler):
    def get(self):
        self.render('playit.html')

class SearchHandler(tornado.web.RequestHandler):
    def get(self):
        # TODO: Implement Index Handler
        query   = self.get_argument('query', '')
        table   = self.get_argument('table', '')
        results = []

        if table == 'Genre':
            results = self.application.database.genre(query)
        elif table == 'Year':
            results = self.application.database.year(query)
        elif table == 'Name':
            results = self.application.database.name(query)

        self.write(json.dumps({
            'render' : 'gallery',
            'prefix' : table[:-1],
            'results': list(results),
        }
        ))

class GenreHandler(tornado.web.RequestHandler):
    def get(self, artist_id=None):
        # TODO: Implement Artist Handler
        if not artist_id:
            self.write(json.dumps({
                'render' : 'gallery',
                'prefix' : 'genre',
                'results': list(self.application.database.genre('')),
            }))
        else:
            self.write(json.dumps({
                'render' : 'genre',
                'results': list(self.application.database.genre(artist_id)),
            }))

class YearHandler(tornado.web.RequestHandler):
    def get(self, album_id=None):
        # TODO: Implement Album Handler
        if not album_id:
            self.write(json.dumps({
                'render' : 'gallery',
                'prefix' : 'year',
                'results': list(self.application.database.year('')),
            }))
        else:
            self.write(json.dumps({
                'render' : 'year',
                'results': list(self.application.database.year(album_id)),
            }))

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
