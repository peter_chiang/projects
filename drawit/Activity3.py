#!/usr/bin/env python

from drawit import Image, Point, PPM, RED, GREEN, BLUE

image  = Image()

image.draw_rectangle(
    Point(0, 0),
    Point(image.width/10, image.height - 1),
    RED
)

# Upper Honizontal
image.draw_rectangle(
    Point( image.width/10, 0),
    Point(2*image.width / 5, image.height/10),
    RED
)
#Lower
image.draw_rectangle(
    Point( image.width/10, 4*image.height/10),
    Point(2*image.width / 5, image.height/2),
	RED
)

#Right Vert
image.draw_rectangle(
    Point(3*image.width / 10, image.height / 10),
    Point(4*image.width / 10, 5*image.height / 10),
    RED
)


#C
image.draw_rectangle(
    Point(image.width/2,0),
    Point(image.width/2+image.width/10, image.height - 1),
    GREEN
)

image.draw_rectangle(
    Point(image.width/2+image.width/10, 0),
    Point(image.width, image.height/10),
    GREEN
)

image.draw_rectangle(
    Point(image.width/2, 9*image.height/10),
    Point(image.width, image.height),
	GREEN
)
# Write image to stdout
PPM.write(image)

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
