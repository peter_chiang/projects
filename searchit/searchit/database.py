''' searchit.database - SQLite Database '''

import logging
import sqlite3
import yaml

# Database

class Database(object):
	SQLITE_PATH = 'searchit.db'
	YAML_PATH   = 'assets/yaml/data.yaml'

	def __init__(self, data=YAML_PATH, path=SQLITE_PATH):
		# TODO: Set instance variables and call _create_tables and _load_tables
		self.logger = logging.getLogger()
		self.data   = data
		self.path   = path
		self.conn   = sqlite3.connect(path)

		self._create_tables()
		self._load_tables()

	def _create_tables(self):
		# TODO: Create Artist, Album, and Track tables
		with self.conn:
			cursor = self.conn.cursor()
			artists_sql = '''
			CREATE TABLE IF NOT EXISTS Artists(
				ArtistID INTEGER NOT NULL,
				Name TEXT NOT NULL,
				Image TEXT NOT NULL,
				PRIMARY KEY(ArtistID)
			)
			'''
			cursor.execute(artists_sql)
			albums_sql = '''
			CREATE TABLE IF NOT EXISTS  Album(
				ArtistID INTEGER NOT NULL,
				AlbumID INTEGER NOT NULL,
				Name TEXT NOT NULL,
				Image TEXT NOT NULL,
				PRIMARY KEY(AlbumID)
				FOREIGN KEY(ArtistID) References Artists(ArtistID)
			)
			'''
			cursor.execute(albums_sql)

			tracks_sql = '''
			CREATE TABLE IF NOT EXISTS Tracks(
				AlbumID INTEGER NOT NULL,
				TrackID INTEGER NOT NULL,
				Number INTEGER NOT NULL,
				Name TEXT NOT NULL,
				PRIMARY KEY(TrackID)
				FOREIGN KEY(AlbumID) References Album(AlbumID)
			)
			'''


			cursor.execute(tracks_sql)


	def _load_tables(self):
		# TODO: Insert Artist, Album, and Track tables from YAML
	
		cursor = self.conn.cursor()
		ArtistID = 0 
		TrackID  = 0
		AlbumID  = 0
		for artist in yaml.load(open(self.data)):
			artist_sql = 'INSERT OR REPLACE INTO Artists(ArtistID, Name, Image) VALUES(?, ?, ?)'
			ArtistID += 1 
			Name = artist['name']
			Image = artist['image']
			cursor.execute(artist_sql,(ArtistID,Name,Image))
			self.logger.info('Inserted Artist: ArtistID={}, Name={}, Image={}'.format(ArtistID, Name, Image))
	
			for album in artist['albums']:
				album_sql  ='INSERT OR REPLACE INTO Album(ArtistID, AlbumID, Name, Image) VALUES(?, ?, ?, ?)'
				AlbumID += 1
				Name = album['name']
				Image = album['image']
				cursor.execute(album_sql,(ArtistID,AlbumID,Name,Image))
				self.logger.info('Inserted Album: ArtistID={}, AlbumID={}, Name={}, Image={}'.format(ArtistID, AlbumID, Name, Image))

				Number=0
				for track in album['tracks']:
					track_sql  ='INSERT OR REPLACE INTO Tracks(AlbumID, TrackID, Number, Name)VALUES(?, ?, ?, ?)'
					TrackID += 1
					Number +=1
					trackname=track
					cursor.execute(track_sql,(AlbumID,TrackID,Number,trackname))
					self.logger.info('Inserted Track: AlbumID={}, TrackID={} Number={}, Name={}'.format(AlbumID, TrackID, Number, trackname))

		
		

	def artists(self, artist):
		# TODO: Select artists matching query
		with self.conn:
			cursor = self.conn.cursor()
			artists_sql = '''
			SELECT ArtistID, Name, Image
			FROM Artists
			WHERE Name Like ?
			'''
			return cursor.execute(artists_sql, ('%'+artist+'%',))



	def artist(self, artist_id=None):
		# TODO: Select artist albums
		with self.conn:
			cursor = self.conn.cursor()
			artist_sql = '''
			SELECT Album.AlbumID, Album.Name, Album.Image
			FROM Artists
			JOIN Album ON Album.ArtistID =Artists.ArtistID
			WHERE Album.ArtistID = ?

			'''

			return cursor.execute(artist_sql, (artist_id,))



	def albums(self, album):
		# TODO: Select albums matching query
		with self.conn:
			cursor = self.conn.cursor()
			albums_sql = '''
			SELECT AlbumID, Name, Image
			FROM Album
			WHERE Name LIKE ?
			'''

			return cursor.execute(albums_sql,('%'+album+'%',))


	def album(self, album_id=None):
		# TODO: Select specific album
		with self.conn:
			cursor = self.conn.cursor()
			album_sql = '''
			SELECT Tracks.TrackID, Tracks.Number, Tracks.Name 
			From Album
			JOIN Tracks ON Tracks.AlbumID = Album.AlbumID
			Where Tracks.AlbumID = ?
			'''

			return cursor.execute(album_sql,(album_id,))
		

	def tracks(self, track):
		# TODO: Select tracks matching query
		with self.conn:
			cursor = self.conn.cursor()
			tracks_sql = '''
			SELECT Tracks.TrackID, Tracks.Name, Album.Image
			From Tracks
			JOIN Album ON Tracks.AlbumID = Album.AlbumID
			Where Tracks.Name LIKE ?
			'''
			print track
			return cursor.execute(tracks_sql,(track,))
		

	def track(self, track_id=None):
		# TODO: Select specific track
		with self.conn:
			cursor = self.conn.cursor()
			track_sql = '''
			SELECT TrackID, Artists.ArtistID, Artists.Name, Album.AlbumID, Album.Name, Number, Tracks.Name
			From Tracks
			JOIN Album ON Tracks.AlbumID = Album.AlbumID
			JOIN Artists ON Album.ArtistID = Artists.ArtistID
			WHERE TrackID = ?
			'''

			return cursor.execute(track_sql,(track_id,))
		
# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
