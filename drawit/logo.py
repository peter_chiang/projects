#!/usr/bin/env python

from drawit import Image, Color, Point, PPM
import math
import random


#Colors
TAN       = Color(255, 231, 186)
CHOCOLATE = Color(205, 149, 12)
PINK      = Color(255, 192, 203)
WHITE     = Color(255, 255, 255)


image   = Image()
center  = Point(image.width / 2, image.height / 1.8)
center2 = Point(image.width / 2, image.height / 2.6)
center3 = Point(image.width / 2, image.height / 4.5)
radius  = min(image.width, image.height) / (6 - 1)



# Strawberry
image.draw_circle(center, radius/2, PINK)

# Chocolate
image.draw_circle(center2, radius/2, CHOCOLATE)

# Vanilla
image.draw_circle(center3, radius/2, WHITE)

# Top cone
image.draw_rectangle(
    Point(  image.width / 3, 5*image.height / 8),
    Point(2*image.width / 3, 2*image.height / 3), TAN
)
# Bottom Cone
image.draw_rectangle(
    Point(2*image.width /5 , 2*image.height / 3),
    Point(29*image.width / 50, 22*image.height / 25), TAN
)


# Write image to stdout
PPM.write(image)

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:

