#!/usr/bin/env python

from drawit import Image, Point, PPM, RED, GREEN, BLUE,YELLOW,BLACK, MAGENTA

image  = Image()

import math
import random

image       = Image()
center      = Point(image.width / 2, image.height / 2)
mindim      = min(image.width, image.height)
colors      = [RED, GREEN, BLUE,YELLOW,BLACK, MAGENTA]
color_index = 0
angle       = 0

# Draw lines from center
for i in range(500):
        #radius=(random.randint(0,mindim/10))
    radius=50
    startingpoint=Point(random.randint(0,mindim),random.randint(0,mindim))
    edge = Point(center.x + radius*math.cos(angle),
                 center.y + radius*math.sin(angle))
    image.draw_line(startingpoint, edge,colors[random.randint(0,len(colors)-1)])
    if angle <= (2*math.pi):
    	angle       += math.pi / 6
    else:
    	angle       = 0

# Write image to stdout
PPM.write(image)


# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
