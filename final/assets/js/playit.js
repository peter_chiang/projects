/* Globals */

var PlaylistQueue   = [];	    /* List of songs	*/
var PlaylistIndex   = -1;	    /* Current song	*/
var CurrentNotebook = 'Search';	    /* Current notebook */

/* AJAX functions */

function requestJSON(url, callback) {
    var request = new XMLHttpRequest();
    request.onreadystatechange = function() {
    	if (request.readyState == 4 && request.status == 200) {
    	    callback(JSON.parse(request.responseText));
	}
    }

    request.open('GET', url, true);
    request.send();
}

/* Display functions */

function displayTab() {
    /* TODO: Remove the active class from each notebook tab */
    

    /* TODO: Add active class to current object */

    /* TODO: Check current object's id and call the appropriate display function */
    for (var tags = document.getElementsByClassName("nav-tabs")[0].getElementsByTagName("li"), t = 0; t < tags.length; t++) {
        console.log("grabbed")
        var temp = tags[t];
        temp.classList.remove("active")
    }
    this.classList.add("active");
    var b = this.getElementsByTagName('a')[0].innerHTML;
    if  (b=="Search") {
            displaySearch();
            }
    else if(b=="Genre"){
            displaygenre();
            }
    else if(b=="Year"){
            displayyear();
            }

}

function displayResults(url) {
    /* TODO: Request JSON from URL and then call appropriate render function */
    a=requestJSON(url, function(data) {
        if (data['render']=="gallery"){
            renderGallery(data['results'],data['prefix']);
        } else if (data['render']=="year"){
            renderYear(data['results']);
        } else if (data['render']=="genre"){
            renderGenre(data['results']);
        }
    })
  }

function displaySearch(query) {
    /* TODO: Set CurrentNotebook */

    /* TODO: Set notebook-body-nav's innerHTML to display search form */

    /* TODO: Call updateSearchResults */
    CurrentNotebook = "Search";
    var element = document.getElementById("notebook-body-nav");
    var renderHTML='<form class="navbar-form text-center">';
    renderHTML += '<div class="form-group">'
    renderHTML += '<label class="control-label sr-only">Title</label> <input type="text" id="query" name="query" class="form-control" placeholder="Search" onkeyup="updateSearchResults()"></div><div class="form-group">'
    renderHTML += '<select id="table" name="table" class="form-control" onchange="updateSearchResults()">'
    renderHTML += '<option value="Name">Name</option>'
    renderHTML += '<option value="Genre">Genre</option>'
    renderHTML += '<option value="Year">Year</option>'
    renderHTML += '</select>'
    renderHTML += '</div></form>' 

    element.innerHTML = renderHTML;
    updateSearchResults();
}


function displaygenre(genre) {
    /* TODO: Set CurrentNotebook */

    CurrentNotebook = "Genre";

    /* TODO: Set notebook-body-nav's innerHTML to empty string or some other useful information */
    var element = document.getElementById("notebook-body-nav")
    if (genre ===undefined){
        genre='';
    }
    /* TODO: If artist is undefined, then set to empty string */

    /* TODO: Call displayResults for appropriate /artist/ URL */
    displayResults("genre/"+genre)
}

function displayyear(year) {
    /* TODO: Set CurrentNotebook */
    CurrentNotebook = "Year";
    /* TODO: Set notebook-body-nav's innerHTML to empty string or some other useful information */
    var element = document.getElementById("notebook-body-nav")
    if (year ===undefined){
        year='';
    }
    /* TODO: If album is undefined, then set to empty string */
    displayResults("year/"+year)
    /* TODO: Call displayResults for appropriate /album/ URL */
}
function displayname(neam) {
    /* TODO: Set CurrentNotebook */
    CurrentNotebook = "Name";
    /* TODO: Set notebook-body-nav's innerHTML to empty string or some other useful information */
    var element = document.getElementById("notebook-body-nav")
    if (name ===undefined){
        name='';
    }
    /* TODO: If album is undefined, then set to empty string */
    displayResults("name/"+name)
    /* TODO: Call displayResults for appropriate /album/ URL */
}


function renderGallery(data, prefix) {
    var rend = document.getElementById("notebook-body-contents"),
        result = '<div class="row">';
    for (var item in data){
        var actitem= data[item]
        result+='</div><div class="col-xs-3">'
        result+='<a href=\"'+actitem[3]+'" target="_blank" onclick=\"display'+prefix+'(' +actitem[4]+')'+'\"'+ 'class="thumbnail text-center" >'
        result+='<img src='+ actitem[1]+ ' height="600" width="400">'
        result +='<div class="caption"><h4>'+actitem[0]+'</h4></div></a></div>'
        }
            result += '</div>';
    rend.innerHTML=result
    }


function renderGenre(data) {
 var rend = document.getElementById("notebook-body-contents"),
        result = '<table class="table table-striped"><thead><th>Number</th><th>Name</th><th>Actions</th></thead><tbody>';
    for (var item in data){
        var actitem= data[item]
        result+='<tr><td>' + actitem[0] + '</td><td><a href="'+actitem[3]+'" >'+ actitem[4]+ '</a></td><td>'
        }
            result += '</td></tr></tbody></table>';
    rend.innerHTML=result
    }

function renderYear(data) {
 var rend = document.getElementById("notebook-body-contents"),
        result = '<table class="table table-striped"><thead><th>Name</th><th>Genre</th><th>Trailer</th></thead><tbody>';
    for (var item in data){
        var actitem= data[item]
        result+='<tr><td>' + actitem[0] + '</td><td><a href="'+actitem[3]+'">'+ actitem[4]+ '</a></td><td>'
        }
            result += '</td></tr></tbody></table>';
    rend.innerHTML=result
    }

function renderName(data) {
 var rend = document.getElementById("notebook-body-contents"),
        result = '<table class="table table-striped"><thead><th>Name</th><th>Genre</th><th>Trailer</th></thead><tbody>';
    for (var item in data){
        var actitem= data[item]
        result+='<tr><td>' + actitem[0] + '</td><td><a href="'+actitem[3]+'">'+ actitem[4]+ '</a></td><td>'
        }
            result += '</td></tr></tbody></table>';
    rend.innerHTML=result
    }



/* Update functions */

function updateSearchResults() {
    /* TODO: Construct search URL from query and table values and call
     * displayResults
     */
    var query = document.getElementById("query").value
    var table = document.getElementById("table").value
    var result     = '/search?query=' + query + '&table=' + table 
    displayResults(result)


}

/* Registrations */
window.onload = function() {
for (var tags = document.getElementsByClassName("nav-tabs")[0].getElementsByTagName("li"), t = 0; t < tags.length; t++) {
        var a = tags[t];
        a.onclick = displayTab
    }
    displaySearch()
};
