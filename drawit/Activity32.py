#!/usr/bin/env python

from drawit import Image, Point, PPM, RED, GREEN, BLUE,YELLOW,BLACK, MAGENTA
import math
image  = Image()
center = Point(image.width / 2, image.height / 2)
eye1= Point(image.width / 2+ image.width /5, image.height / 4)
eye2=Point(image.width / 2- image.width /5, image.height / 4)
radius = min(image.width, image.height) / 2 - 1

# Big Red
image.draw_circle(center, radius, YELLOW)

# Medium Green
image.draw_circle(eye1, radius/8, BLACK)

# Small Blue
image.draw_circle(eye2, radius/8, BLACK)

#Mouth

i=0
while i <=math.pi-0.1:
    dist=image.height/3
    newcenter=Point(center.x+dist*math.cos(i),center.y+dist*math.sin(i))
    image.draw_circle(newcenter, radius/8, MAGENTA)
    i+=0.1

image.draw_rectangle(
    Point( image.width/2-dist-radius/8, image.height/2-image.height/20),
    Point( image.width/2+dist+radius/8-20, image.height/2+image.height/20),
    MAGENTA
)
# Write image to stdout
PPM.write(image)

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
