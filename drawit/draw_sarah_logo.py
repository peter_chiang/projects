#!/usr/bin/env python

from drawit import Image, Color, Point, PPM

import math
import random

#Colors
PURPLE  = Color(72, 61, 139)
YELLOW  = Color(255, 215, 0)
GREEN   = Color(0,0,255)

image  = Image()

radius = min(image.width, image.height) / 10 - 1
radius1 = radius * 2

center = Point(image.width / 2, image.height / 2)
center1 = Point(image.width/ 2 + radius + radius1, image.height / 2)
center2 = Point(image.width/ 2 - radius - radius1, image.height / 2)
center3 = Point(image.width / 2, image.height / 2 + radius + radius1)
center4 = Point(image.width / 2, image.height / 2 - radius - radius1)


#Draw center of flower
image.draw_circle(center, radius, YELLOW)

#Draw purple petals

image.draw_circle(center1, radius1, PURPLE)
image.draw_circle(center2, radius1, PURPLE)
image.draw_circle(center3, radius1, PURPLE)
image.draw_circle(center4, radius1, PURPLE)

# Draw stem
#point0 =
#point1 =
#color  = GREEN

#image.draw_rectangle(point0,point1,color)

PPM.write(image)