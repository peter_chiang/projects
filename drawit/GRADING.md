Project 1 - Grading
-------------------

Group Score: 27/27

Feedback on code quality:

Your code is very clean and readable. Great job. In your impage.py file, I see one possible problem with your draw-Rectangle fuunction. What happens if point1.x is less than point0.x or point1.y is less than point0.y? You may run into out of bounds problems. Something else to consider is adding a few more comments to explain what you are doing. For example, your draw_line function in image.py and your distance_from function in point.py could benefit from a comment that explains the math being done.

Overall, well done!

Individual Work:

Pchiang Score: 3/3

Jwilso31 Score: 3/3

Swyczaws Score: 3/3

Feedback on individual code quality:

All of you have clean code. Two suggestions that apply to all of you:

1. Add a few more comments describing what's going on
2. Try not to hard-code values. Instead, when setting a height, width, radius, etc., try to use a percentage of the total image height and width. That way, if you change the image size, you won't have to re-calculate and change every value.
