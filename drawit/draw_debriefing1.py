#!/usr/bin/env python

from drawit import Image, Point, PPM, Color

#Colors
RED     = Color(255, 0, 0)
ORANGE  = Color(255, 128, 0)
YELLOW  = Color(255, 255, 0)
GREEN   = Color(0, 255, 0)
BLUE    = Color(0, 0, 255)
INDIGO  = Color(25, 25, 112)
VIOLET  = Color(138, 43, 226)
SKY     = Color(240, 255, 255)


image    = Image()
center   = Point(image.width / 2, image.height / 1.01)
radius   = min(image.width, image.height) / (2 - 1)


image.draw_rectangle(
    Point(  0, 0),
    Point(image.width, image.height), SKY
)

image.draw_rectangle(
    Point(  0, 0),
    Point(image.width, image.height), SKY
)

image.draw_square(Point(0,0), 80, RED)




# Write image to stdout
PPM.write(image)

# vim: set sts=4 sw=4 ts=8 expandtab ft=python: