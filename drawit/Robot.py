#!/usr/bin/env python

from drawit import Image, Point, PPM, RED, GREEN, BLUE,YELLOW,BLACK, MAGENTA

image  = Image()
center = Point(image.width / 2, image.height / 2)
eye1= Point(image.width / 2+ image.width /5, image.height / 4)
eye2=Point(image.width / 2- image.width /5, image.height / 4)
radius = min(image.width, image.height) / 2 - 1

# Big Red
image.draw_circle(center, radius, YELLOW)

# Medium Green
image.draw_circle(eye1, radius/8, BLACK)

# Small Blue
image.draw_circle(eye2, radius/8, BLACK)

#Mouth
image.draw_rectangle(
    Point( image.width/2-image.width/5, 2*image.height/10),
    Point( image.width/2+image.width/5, 3*image.height/10),
	BLUE
)

image.draw_rectangle(
    Point( image.width/2-image.width/5, 4*image.height/10),
    Point( image.width/2+image.width/5, 8*image.height/10),
	RED
)

image.draw_rectangle(
    Point( image.width/2+image.width/4, 4*image.height/10),
    Point( image.width/2+image.width/3.5, 8*image.height/10),
	RED
)
image.draw_rectangle(
    Point( image.width/2-image.width/3.5, 4*image.height/10),
    Point( image.width/2-image.width/4, 8*image.height/10),
    RED
)
# Write image to stdout
PPM.write(image)

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
