from drawit import Image, Color, Point, PPM, WHITE, BLACK, RED

import math
import random

#!/usr/bin/env python

from drawit import Image, Point, PPM, RED, GREEN, BLUE

image   = Image()
center  = Point(image.width / 2, image.height / 2)
center2 = Point(image.width / 3, image.height / 3)
center3 = Point(image.width / 4, image.height / 4)
radius  = min(image.width, image.height) / 2 - 1

# Big Red
image.draw_circle(center, radius, RED)

# Medium Green
image.draw_circle(center2, radius/1.5, GREEN)

# Small Blue
image.draw_circle(center3, radius/2, BLUE)

# Write image to stdout
PPM.write(image)

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
