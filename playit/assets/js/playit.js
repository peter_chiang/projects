/* Globals */

var PlaylistQueue   = [];	    /* List of songs	*/
var PlaylistIndex   = -1;	    /* Current song	*/
var CurrentNotebook = 'Search';	    /* Current notebook */

/* AJAX functions */

function requestJSON(url, callback) {
    var request = new XMLHttpRequest();
    request.onreadystatechange = function() {
    	if (request.readyState == 4 && request.status == 200) {
    	    callback(JSON.parse(request.responseText));
	}
    }

    request.open('GET', url, true);
    request.send();
}

/* Display functions */

function displayTab() {
    /* TODO: Remove the active class from each notebook tab */
    

    /* TODO: Add active class to current object */

    /* TODO: Check current object's id and call the appropriate display function */
    for (var tags = document.getElementsByClassName("nav-tabs")[0].getElementsByTagName("li"), t = 0; t < tags.length; t++) {
        console.log("grabbed")
        var temp = tags[t];
        temp.classList.remove("active")
    }
    this.classList.add("active");
    var b = this.getElementsByTagName('a')[0].innerHTML;
    if  (b=="Search") {
            console.log("changed")
            displaySearch();
            }
    else if(b=="Artists"){
      console.log("changed")
            displayArtist();
            }
    else if(b=="Albums"){
            displayAlbum();
            }
    else if(b=="Tracks"){
            displayTrack();
            }
    else if(b=="Playlist"){
            displayPlaylist();
            
    }
}

function displayResults(url) {
    /* TODO: Request JSON from URL and then call appropriate render function */
    a=requestJSON(url, function(data) {
        if (data['render']=="gallery"){
            renderGallery(data['results'],data['prefix']);
        } else if (data['render']=="album"){
            renderAlbum(data['results']);
        } else if (data['render']=="track"){
            renderTrack(data['results']);
        }
    })
  }

function displaySearch(query) {
    /* TODO: Set CurrentNotebook */

    /* TODO: Set notebook-body-nav's innerHTML to display search form */

    /* TODO: Call updateSearchResults */
    CurrentNotebook = "Search";
    var element = document.getElementById("notebook-body-nav");
    var renderHTML='<form class="navbar-form text-center">';
    renderHTML += '<div class="form-group">'
    renderHTML += '<label class="control-label sr-only">Title</label> <input type="text" id="query" name="query" class="form-control" placeholder="Search" onkeyup="updateSearchResults()"></div><div class="form-group">'
    renderHTML += '<select id="table" name="table" class="form-control" onchange="updateSearchResults()">'
    renderHTML += '<option value="Artists">Artists</option>'
    renderHTML += '<option value="Albums">Albums</option>'
    renderHTML += '<option value="Tracks">Tracks</option>'
    renderHTML += '</select>'
    renderHTML += '</div></form>' 

    element.innerHTML = renderHTML;
    updateSearchResults();
}


function displayArtist(artist) {
    /* TODO: Set CurrentNotebook */

    CurrentNotebook = "Artist";

    /* TODO: Set notebook-body-nav's innerHTML to empty string or some other useful information */
    var element = document.getElementById("notebook-body-nav")
    if (artist ===undefined){
        artist='';
    }
    /* TODO: If artist is undefined, then set to empty string */

    /* TODO: Call displayResults for appropriate /artist/ URL */
    displayResults("artist/"+artist)
}

function displayAlbum(album) {
    /* TODO: Set CurrentNotebook */
    CurrentNotebook = "Albums";
    /* TODO: Set notebook-body-nav's innerHTML to empty string or some other useful information */
    var element = document.getElementById("notebook-body-nav")
    if (album ===undefined){
        album='';
    }
    /* TODO: If album is undefined, then set to empty string */
    displayResults("album/"+album)
    /* TODO: Call displayResults for appropriate /album/ URL */
}

function displayTrack(track) {
    /* TODO: Set CurrentNotebook */
    CurrentNotebook= "Tracks"
    /* TODO: Set notebook-body-nav's innerHTML to empty string or some other useful information */
    var element = document.getElementById("notebook-body-nav")
    if (track ===undefined){
        track='';
    }
    /* TODO: If album is undefined, then set to empty string */
    displayResults("track/"+track)
    /* TODO: Call displayResults for appropriate /album/ URL */
}

function displayPlaylist() {
    // /* TODO: Set CurrentNotebook */
    // CurrentNotebook = "Playlist"
    // /* TODO: Set notebook-body-nav's innerHTML to empty string or some other useful information */
    // var element = document.getElementById("notebook-body-contents")
    // var renderHTML='<form class="navbar-form text-center" id="searchform">'
    // renderHTML += '<div class="form-group">'
    // renderHTML += '<input type="text" name="firstname">'
    // renderHTML += '<select id="searchselect">'
    // renderHTML += '<option value="Artist">Artists</option>'
    // renderHTML += '<option value="Album">Albums</option>'
    // renderHTML += '<option value="Track">Tracks</option>'
    // renderHTML += '</select>'
    // renderHTML += '</div></form>' 

    CurrentNotebook = "Playlist";
    var rend = document.getElementById("notebook-body-contents"),
        result = '<table id="playlist" class="table table-striped"><thead><th>#</th><th></th><th>Artist</th><th>Album</th><th>Track</th></thead><tbody>';
    for (var a in PlaylistQueue) {
        var song = PlaylistQueue[a],
            n = parseInt(a) + 1;
        result += a == PlaylistIndex ? '<tr class="playlist-active">' : "<tr>";
        result += "<td>" + n + '</td><td><img src="' + song['albumImage'] + '" class="img-rounded playlist-image"/></td>';
        result += "<td>" + song['artistName'] + "</td><td>" + song['albumName'] + "</td><td>" + song['trackName'] + "</td></tr>"
    }
    result += "</tbody></table>";
    rend.innerHTML = result
}
    /* TODO: Set notebook-body-contents to playlist table
     *
     *  - Table should have playlist identifier.
     *  - Table should have columns for playlist index, album cover, artist, album, and track names.
     *  - Table should highlight the active song.
     *  - Table should be generated by looping through objects in PlaylistQueue.
     *
     *  - Optional: add callback so that if user clicks on song, then the song is played.
     */

/* Render functions */

function renderGallery(data, prefix) {
    var rend = document.getElementById("notebook-body-contents"),
        result = '<div class="row">';
    for (var item in data){
        var actitem= data[item]
        result+='</div><div class="col-xs-3">'
        result+='<a href=\"#\" onclick=\"display'+prefix+'(' +actitem[0]+')'+'\"'+ 'class="thumbnail text-center" >'
        result+='<img src='+ actitem[2]+ '>'
        result +='<div class="caption"><h4>'+actitem[1]+'</h4></div></a></div>'
        }
            result += '</div>';
    rend.innerHTML=result
    }


function renderAlbum(data) {
 var rend = document.getElementById("notebook-body-contents"),
        result = '<table class="table table-striped"><thead><th>Number</th><th>Name</th><th>Actions</th></thead><tbody>';
    for (var item in data){
        var actitem= data[item]
        result+='<tr><td>' + actitem[1] + '</td><td><a href="#" onclick= "displayTrack(' + actitem[0] + ')">'+ actitem[2]+ '</a></td><td>'
        result+='<a href="#" onclick=" addSong(' + actitem[0] + ')"><i class="fa fa-plus"></i></a> '
        result+='<a href="#" onclick=" playSong(' + actitem[0] + ')"><i class="fa fa-play"></i></a>'
        }
            result += '</td></tr></tbody></table>';
    rend.innerHTML=result
    }

function renderTrack(data) {
var rend = document.getElementById("notebook-body-contents");
    var result='<dl><div class="row"><div class="col-sm-3">';
    result+='<a href="#" onclick="playSong"('+data[0]+')" class="thumbnail text-center"><img src='+data[7]+'></a>';
  result += '<div class="col-sm-6"><dl><dt>Track ID</dt><dd>'+data[0]+'</dd>'
  result+='<dt>Artist</dt><dd><a href="#" onclick="displayArtist"('+data[1]+')">'+data[2]+'</a></dd>'
  result+='<dt>Album</dt>'
  result+='<dd><a href="#" onclick="displayAlbum"('+data[3]+')">'+data[4]+'</a></dd>'
  result+='<dt>Track Number</dt><dd>'+data[5]+'</dd><dt>Track Name</dt><dd>'+data[6]+'</dd></dl>'
  result+='</div></div></dl>'
rend.innerHTML=result

}



/* Update functions */

function updateSearchResults() {
    /* TODO: Construct search URL from query and table values and call
     * displayResults
     */
    var query = document.getElementById("query").value
    var table = document.getElementById("table").value
    var result     = '/search?query=' + query + '&table=' + table 
    displayResults(result)

}

function updateSongInformation() {
    /* TODO: Get current song from PlaylistQueue and update the following elements:
     *
     *	1. trackName.innerHTML
     *	2. albumName.innerHTML
     *	3. artistName.innerHTML
     *	4. albumImage.src
     *	5. player.src
     *
     *	If CurrentNotebook is Playlist, then call displayPlaylist.
     */
     var trackName        = document.getElementById('trackName')
     var albumName        = document.getElementById('albumName')
     var artistName       = document.getElementById('artistName')
     var albumImage       = document.getElementById('albumImage')
     var player           = document.getElementById('player')
     var top              = PlaylistQueue[PlaylistIndex]
     trackName.innerHTML  = top['trackName']
     albumName.innerHTML  = top['albumName']
     artistName.innerHTML = top['artistName']
     albumImage.src       = top['albumImage']
     player.src           = top['songURL']
     
     if (CurrentNotebook == 'Playlist'){
      displayPlaylist();
    }
}


/* Audio controls */

function addSong(trackId, select) {
    var url = "/song/" + trackId;

    requestJSON(url, function(data) {
	var song = data['song'];
	PlaylistQueue.push(song);

	if (select !== undefined && select) {
	    selectSong(PlaylistIndex + 1);
	}
    });
}

function prevSong() {
    if (PlaylistIndex < 0 || PlaylistQueue.length == 0) {
    	return;
    }

    selectSong((PlaylistIndex + PlaylistQueue.length - 1) % PlaylistQueue.length);
}

function nextSong() {
    if (PlaylistIndex < 0 || PlaylistQueue.length == 0) {
    	return;
    }

    selectSong((PlaylistIndex + 1) % PlaylistQueue.length);
}

function selectSong(index) {
    PlaylistIndex = index;
    updateSongInformation();
    playSong();
}

function playSong(trackId) {
    var player     = document.getElementById('player');
    var playButton = document.getElementById('playButton');

    if (trackId !== undefined && trackId != '') {
    	return addSong(trackId, true);
    }

    if (PlaylistIndex < 0 && PlaylistQueue.length) {
    	return selectSong(0);
    }

    if (player.src === undefined || player.src == '') {
    	return;
    }

    if (player.ended || player.paused) {
    	playButton.innerHTML = '<i class="fa fa-pause"></i>';
	player.play();
    } else {
    	playButton.innerHTML = '<i class="fa fa-play"></i>';
	player.pause();
    }
}

function togglePlaySong() {
    playSong('');
}

function endSong() {
    document.getElementById('playButton').innerHTML = '<i class="fa fa-play"></i>';
    nextSong();
}

/* Registrations */
window.onload = function() {
for (var tags = document.getElementsByClassName("nav-tabs")[0].getElementsByTagName("li"), t = 0; t < tags.length; t++) {
        var a = tags[t];
        a.onclick = displayTab
    }
    document.getElementById("prevButton").onclick = prevSong;
    document.getElementById("playButton").onclick = togglePlaySong;
    document.getElementById("nextButton").onclick = nextSong;
    displaySearch()
};
