''' playit.database - SQLite Database '''

import logging
import sqlite3
import yaml

# Database

class Database(object):
    SQLITE_PATH = 'playit.db'
    YAML_PATH   = 'assets/yaml/trailers.yaml'

    def __init__(self, data=YAML_PATH, path=SQLITE_PATH):
        self.logger = logging.getLogger()
        self.data   = data
        self.path   = path
        self.conn   = sqlite3.connect(self.path)

        self._create_tables()
        self._load_tables()

    def _create_tables(self):
        movies_sql = '''
        CREATE TABLE IF NOT EXISTS Movies (
            Name        TEXT NOT NULL,
            Image       TEXT    NOT NULL,
            Genre       TEXT    NOT NULL,
            URL         TEXT    NOT NULL,
            Year        INTEGER NOT NULL,
            PRIMARY KEY (Name)
        )
        '''
        with self.conn:
            cursor = self.conn.cursor()
            cursor.execute(movies_sql)

    def _load_tables(self):
        movies_sql = 'INSERT OR REPLACE INTO Movies (Name, Image, Genre, URL, Year) VALUES (?, ?, ?, ?, ?)'

        with self.conn:
            cursor    = self.conn.cursor()


            for movie in yaml.load(open(self.data)):
                cursor.execute(movies_sql, (movie['name'], movie['image'],movie['genre'],movie['url'],movie['year']))


    def name(self, name=None):
        name_sql = '''
        SELECT Name, Image, Genre, URL, Year 
        FROM Movies
        WHERE Name LIKE ? 
        ORDER BY Name
        '''
        
        with self.conn:
            cursor = self.conn.cursor()
            return cursor.execute(name_sql, ('%{}%'.format(name),))

    def genre(self, genre=None):
        artist_sql = '''
        SELECT      Name, Image, Genre, URL, Year 
        FROM        Movies
        WHERE       Genre LIKE ?
        ORDER BY    Name
        '''
        with self.conn:
            cursor = self.conn.cursor()
            return cursor.execute(artist_sql, ('%{}%'.format(genre),))

    def year(self, year=None):
        albums_sql = '''
        SELECT Name, Image, Genre, URL, Year 
        FROM Movies
        WHERE Year LIKE ? 
        ORDER BY Name
        '''
        
        with self.conn:
            cursor = self.conn.cursor()
            return cursor.execute(albums_sql, ('%{}%'.format(year),))
            

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
