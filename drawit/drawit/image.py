''' image.py: DrawIt Image '''

from color import Color
from point import Point

import math

class Image(object):
    DEFAULT_WIDTH  = 640
    DEFAULT_HEIGHT = 480

    def __init__(self, width=DEFAULT_WIDTH, height=DEFAULT_HEIGHT):
        ''' Initialize Image object's instance variables '''
        self.width=int(width)
        self.height=int(height)
        self.pixels=[]
        for i in range(self.height):
            pixels_row=[]
            for j in range(self.width):
                pixels_row.append(Color())
            self.pixels.append(pixels_row)        

    def __eq__(self, other):
        ''' Returns True if current instance and other object are equal '''
        return self.width==other.width and self.height==other.height

    def __str__(self):
        ''' Returns string representing Image object '''
        return 'Image(width={},height={})'.format(self.width,self.height)

    def __getitem__(self, point):
        ''' Returns pixel specified by point
        If point is not with the bounds of the Image, then an IndexError is raised.
        '''
        # TODO
    	if 0<=point.y<=self.height and 0<=point.x<=self.width:
    		return self.pixels[point.y][point.x]
    	else:	
    		raise IndexError("Out of Bound")
		

    def __setitem__(self, point, color):
        ''' Sets pixel specified by point to given color

        If point is not with the bounds of the Image, then an IndexError is raised.
        '''
        # TODO
        if 0<=point.y<=self.height and 0<=point.x<=self.width:
            self.pixels[point.y][point.x]=color
        else:
            raise IndexError("Out of Bound")

    def clear(self):
        ''' Clears Image by setting all pixels to default Color '''
        for i in range(self.height):
		  for j in range(self.width):
			self.pixels[i][j]=Color()
	

    def draw_line(self, point0, point1, color):
        ''' Draw a line from point0 to point1 '''
        # TODO
    	pointydif=point1.y-point0.y
    	pointxdif=point1.x-point0.x
    	angle=math.atan2(pointydif,pointxdif)
    	while angle>=math.pi:
    		angle=angle-math.pi
    	d=Point.distance_from(point0,point1)
        k=0
    	while k <=d:
            k+=0.01
            self[Point(point0.x+k*math.cos(angle),point0.y+k*math.sin(angle))]= color
	

    def draw_rectangle(self, point0, point1, color):
        ''' Draw a rectangle from point0 to point1 '''
        # TODO
        for rectrow in range(point0.x,point1.x):
            for rectcol in range(point0.y,point1.y):
                point=Point(rectrow,rectcol)
                self[point]= color
                
    def draw_circle(self, center, radius, color):
        ''' Draw a circle with center point and radius '''
        for circlerow in range(640):
            for circlecol in range(480):
                point=Point(circlerow,circlecol)
                if math.hypot(point.x-center.x,point.y-center.y)<=radius:
                    self[point]= color
# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
