#!/usr/bin/env python

from drawit import Color

color0 = Color()            # 1
color1 = Color(1, 2, 3)

print color0                # 2
print color1

print color0 == color1      # 3