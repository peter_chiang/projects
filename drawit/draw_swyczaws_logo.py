#!/usr/bin/env python

from drawit import Image, Color, Point, PPM

import math
import random

#Colors
ORCHID  = Color(218,112,214)
YELLOW  = Color(255,246, 143)
MAGENTA   = Color(255,0,255)
BLUE = Color(0,0,255)
GREEN = Color(0,255,127)

image  = Image()

radius = min(image.width, image.height) / 10 - 1
radius1 = radius * 2

center = Point(image.width / 2, image.height / 2)
center1 = Point(image.width/ 2 + radius + radius1, image.height / 2)
center2 = Point(image.width/ 2 - radius - radius1, image.height / 2)
center3 = Point(image.width / 2, image.height / 2 + radius + radius1)
center4 = Point(image.width / 2, image.height / 2 - radius - radius1)


#Draw center of flower
image.draw_circle(center, radius, YELLOW)

# Draw smaller circle

image.draw_circle(center, radius/2, GREEN)

#Draw purple petals

image.draw_circle(center1, radius1, MAGENTA)
image.draw_circle(center2, radius1, MAGENTA)
image.draw_circle(center3, radius1, ORCHID)
image.draw_circle(center4, radius1, ORCHID)



#image.draw_line(point0,point1,color)


pointlr1 = Point(center.x + radius - 14, center.y + radius - 14)
pointul1 = Point(center.x - radius + 14, center.y - radius + 14)
pointll1 = Point(center.x - radius + 14, center.y + radius - 14)
pointur1 = Point(center.x + radius - 14, center.y - radius + 14)

pointlr2 = Point(center1.x + 60, center1.y + radius1 + 100)
pointul2 = Point(center2.x - 60, center2.y - radius1 - 100)
pointll2 = Point(center2.x - 60, center1.y + radius1 + 100)
pointur2 = Point(center1.x + 60, center1.y - radius1 - 100)

image.draw_line(pointlr1,pointlr2, GREEN)
image.draw_line(pointll1,pointll2, GREEN)
image.draw_line(pointul1,pointul2, GREEN)
image.draw_line(pointur1,pointur2, GREEN)



PPM.write(image)