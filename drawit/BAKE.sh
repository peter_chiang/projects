#!/bin/sh

for f in draw_*.py; do
	if [ ! -r $(basename $f .py).ppm ]; then
		echo "Baking $f"
		python $f > $(basename $f .py).ppm 
		convert $(basename $f .py).ppm $(basename $f .py).png
	else
		echo "Skipping $f"
	fi
done
